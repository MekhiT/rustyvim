session_name: Rvim
windows:
- focus: 'true'
  layout: d07d,146x39,0,0,0
  options:
    automatic-rename: 'off'
  panes:
  - focus: 'true'
    shell_command: nvim
  start_directory: /home/mekhi/Programming/Rust/Rvim/src
  window_name: Code
- layout: d07e,146x39,0,0,1
  options:
    automatic-rename: 'off'
  panes:
  - focus: 'true'
    shell_command: zsh
  start_directory: /home/mekhi/Programming/Rust/Rvim
  window_name: Gitlab
