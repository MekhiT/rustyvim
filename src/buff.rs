//this is the code pertaining to the ordering and functionality of buffers in Rvim. Buffers works
//similar to tabs in other editors
use std::collections::VecDeque;
use tui::buffer::Buffer;
use std::mem;
//type Link =  Option<Box <&'static Buff>>; // create an alias for the type Option<Box<Buff>> because that's a lot to type
pub struct BuffList<'a>{
    pub head:  Option<&'a Buffer>,
    pub tail:  Option<&'a Buffer>,
    pub queue: VecDeque<Buffer>,
    pub count: usize, //number of buffers currently active in the list
}//A struct defining the overall list containing each open buffer
/*pub struct Buff{
    data: buffer::Buffer, //will actually hold the buffer for displaying through rust TUI Library
    text: String, //used for testing the LinkedList
    val: u32,
    next: &'static Link,
    prev: &'static Link,
    //TODO: link each buffer to a screen state?
    //TODO: Decide on the concrete functionality of the screen
}//A struct defining what composes each individual buffer
*/
impl BuffList<'_>{

    //Define the proper way to instantiate a new buffer list
    pub fn new() -> Self{
        let ven: VecDeque<Buffer> = VecDeque::new();
        let newList = BuffList{
            queue: ven,
            head: ven.front(),
            tail: ven.back(),
            count: ven.len()
        };
        return newList;
    }
    //Functionality for adding a new buffer to the list, attached to the tail of the list
    //@param the text that will be linked to the buffer, currently placeholder for eventual screen
    //content
    pub fn push(self, input: Buffer){
            self.queue.push_back(input);
        }
    //function for removing a buffer from the list once it is no longer needed
    //TODO: Handle removing from the front, middle and end of list
    //This todo shouldn't be hard once full doubly linked list functionality is implemented
    pub fn pop(&mut self){
        self.queue.pop_back();
    }
    //Find a specific buffer by it's numerical identifier in the list
    //@param: the number of the specified buffer
    pub fn find(&self, target: usize) -> Option<&mut Buffer>{
        if target <= 0 {
            return None;
        }
        else if target > 1 && target < self.count{
            return self.queue.get_mut(target); 
        }
        else{
            return None;
        }
    }
}
/*impl Buff{
    //return the numerical identifier of the current buffer
    fn val(&self) -> u32{
        return self.val;
    }
    //currently returns the string associated with the current buffer
    //TODO: return what ever I decide will represent screen state/content.
    fn getText(&self) -> &str{
       return  self.text.as_str();
    }
    //currently allows you to set the text associated with the current buffer.
    //@para: text to be inserted into buffer
    fn setText(&mut self, input: &str){
        self.text = String::from(input);
    }
    //function to find a specified number according to numerical identifier
    //@param: identity of the desired buffer
    fn find(self, target: u32) -> Link{
        if target == self.val{
            return Some(Box::new(&self));
        }
        else if self.next.is_none(){
            return None;
        }
            return self.next.expect("uh oh").find(target);
    }
}*/

//testing functionality for Buff and BuffList
#[cfg(test)]
mod tests{
//    use super::BuffList;
    #[test]
    fn basics() {
        let mut list = BuffList::new();
        list.pop();
        list.push(Buffer::empty(Rect{x: 0, y: 0, width: 10, height: 5}));
        list.push(Buffer::empty(Rect{x: 2, y:2, width: 10, height: 6}));
        list.push(Buffer::empty(Rect{x:3, y:3, width: 10, height:7}));
        assert_eq!(list.find(0).area(), Rect{x: 0, y: 0, width: 10, height: 5});
        list.pop();
        list.pop();
        assert_eq!(list.find(0).area(), Rect{x: 0, y: 0, width: 10, height: 5});

    }
}
