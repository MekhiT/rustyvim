# Rusty Vim by Mekhi Thomas-El

This is my own attempt at a reimplimentation of vim in rust. This is a personal project spearheaded by me but any and all help and advice is welcome!

## Motivations
The motivations behind this project are my own personal desires to learn. I've recently began learning rust as a programming language and find I generally learn best when I have a project to focus my efforts on. I figured this project would be big enough to give me a full tour of the language. In addition to this I would be able to craft my own version of vim that would fit my needs, hopefully others will find my implementation helpful as well. 

## Initial Features
The following features are what I would consider to be necessary for a minimal viable product
1. Able to create a new file
2. Able to edit an already existing file
3. Able to write back an edited file
4. basic vim style keybindings (e.g. hjkl)

## Planned features
These are features assuming I've compleated all those above
1. Config system
    
    i. this would include rebinding keys
2. Built in layering system for features and possible plugins
    
    i. possibly linked to file type and able to change on buffer
3. Plugin system
4. BIG IF and implementation of the neovim lua system

[Trello](https://trello.com/b/XQyM3gv3/rusty-vim)

Feel free to create an issue if you have an issue :)